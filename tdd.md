### Setup

#### Add testing Gems to your Gemfile
	group :test, :development do
	  gem 'rspec-rails' # testing framework
	  gem 'capybara' # user simulation
	  gem 'guard-rspec' # automatically run tests w/o the need for rake commands
	  gem 'growl' # growl notifications for test results
	end

#### Install the gems
	bundle install

#### Initialize Rspec
	rails generate rspec:install

#### Initialize Guard for auto-testing
	guard init rspec

### Test our setup
	guard

### Create our first test
	rails generate integration_test users

At this point we should have some test that were run, assuming you left Guard running, but they should all fail. This is good.

### Update config/routes.rb
	resources :users

### Generate a controller/action and a model
	rails generate controller Users index
	rails generate model User first_name:string last_name:string email:string

### Create/migrate th database

	rake db:migrate

Let's not forget to migrate our Test database, since this is where the tests actually run.

	rake db:test:prepare

This should be everything you need to get up and running with TDD in Rails. Now write some tests!

### Testing Resources
+	[Net Tuts - The Intro to Rails Screencast I Wish I Had](http://net.tutsplus.com/tutorials/ruby/the-intro-to-rails-screencast-i-wish-i-had/)
+	[RailsCasts - How I Test](http://railscasts.com/episodes/275-how-i-test)
+	[How I learned to test my Rails applications](http://everydayrails.com/2012/03/12/testing-series-intro.html)
+	[Up and Running with Rails and TDD](http://ryanarneson.com/blog/2012/04/07/up-and-running-with-rails-and-tdd/)
+	[Capybara DSL](https://github.com/jnicklas/capybara)

## Rails Resources
+	[http://guides.rubyonrails.org/getting_started.html](http://guides.rubyonrails.org/getting_started.html)
+	[http://net.tutsplus.com/tutorials/ruby/the-best-way-to-learn-ruby-on-rails/](http://net.tutsplus.com/tutorials/ruby/the-best-way-to-learn-ruby-on-rails/)
+	[http://railscasts.com/](http://railscasts.com/)
