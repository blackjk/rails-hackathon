# Rails Hackathon
Below is some information and preliminary instructions on getting Ruby on Rails running on your laptop prior to our hackathon.  We can come around and help all of you get things installed so don't worry.  However, if you have things preinstalled then you will have more time to play around.

## Events
[http://jrubyconf.com/news/2012/03/railsbridge](http://jrubyconf.com/news/2012/03/railsbridge)

# Installation

### Windows
The easiest way to get rails installed on windows is to use the RailsInstaller package.  Download and install with the exe file.

[http://railsinstaller.org/](http://railsinstaller.org/)

### Mac
This guide will help you get ruby 1.9.3 running with rails 3.2

[http://pragmaticstudio.com/blog/2010/9/23/install-rails-ruby-mac](http://pragmaticstudio.com/blog/2010/9/23/install-rails-ruby-mac)


Prerequisites

+   xcode

# Rails Intro

### IDE
While you can use any IDE to develop ruby on rails we prefer Sublime Text 2.  We have plenty of licenses so give it a try.

+   Submlime Text 2
+	Aptana RadRails
+	TextMate

### Rails and databases
By default rails will use a local sqlite database.  Rails can however work with every major database.

### Rails with oracle (Optional)

+	Install Guide - Get instant client (64 BIT) - [here](http://www.pixellatedvisions.com/2009/03/25/rails-on-oracle-part-1-installing-the-oracle-instant-client-on-mac-os-x)
+	[http://rubyforge.org/projects/ruby-oci8](http://rubyforge.org/projects/ruby-oci8)

#### Add to Gemfile
	gem 'ruby-oci8'
	gem 'activerecord-oracle_enhanced-adapter'

#### Install the gems
	bundle install

# Getting started

### Create new project	
	cd ~/Sites # or wherever you want to create a project
	rails new hackathon -T
	cd hackathon

### Fire up server to test
	rails server
Go to http://localhost:3000 in a browser to test. You should see a "Welcome aboard" message.

### Creating hackers
For our hackathon project we should create some hackers.  We will need a model, view and controller.  With rails there is a quick way to do this.  You can generate a scaffold. 

	rails generate scaffold Hacker first_name:string last_name:string email:string

Since rails assumes we are going to want a database table for this hacker object it writes Ruby code to do that in a migration file.  If you look in the db/migrate folder you should see a create hackers file.  This contains instructions to create the hacker table in the database.

To run the script simply issue the statement below.

	rake db:migrate

Just like the "rails" command "rake" will be your new friend.  Think of rake as your personal assistant that performs repetitive tasks.

If all goes well you should see your migration create a new table.

	==  CreateHackers: migrating ==================================================
	-- create_table(:hackers)
	   -> 0.0065s
	==  CreateHackers: migrated (0.0066s) =========================================    

### Viewing our progress

First go into the public folder and delete index.html.  This is the "welcome" page that rails shows by default.

Next, go into the config/routes.rb file.  Add the following line.  This way the root of the site will also point to our "hackers" section.

	root :to => 'hackers#index'

### Seeding our database

Add the following to your db/seeds.rb file.  This will be dummy data that we will "seed" the database with for testing.  Each time you run this file it will reseed the table with this dummy data.

	Hacker.delete_all

	hackers = [
	  {
	    :first_name => 'Jason',
	    :last_name => 'Kadrmas',
	    :email => 'kadrm002@umn.edu'
	  },
	   
	  {
	    :first_name => 'Aaron',
	    :last_name => 'Johnson',
	    :email => 'ajj@umn.edu'
	  },
	   
	  {
	    :first_name => 'Ryan',
	    :last_name => 'Arneson',
	    :email => 'arneson@umn.edu'
	  },

	  {
	    :first_name => 'Ryan',
	    :last_name => 'Ricard',
	    :email => 'rricard@umn.edu'
	  },

	  {
	    :first_name => 'Gyu',
	    :last_name => 'Kwon',
	    :email => 'gyu@umn.edu'
	  }, 

	  {
	    :first_name => 'Barb',
	    :last_name => 'Smith',
	    :email => 'bjs@umn.edu'
	  }, 

	  {
	    :first_name => 'Dalia',
	    :last_name => 'Rashad',
	    :email => 'daila@umn.edu'
	  }
	]

	hackers.each do |hash|
	    hacker = Hacker.new
	    hash.each do |attribute, value|
	      hacker.update_attribute(attribute, value)
	    end
	end

Now we need to use our trusty friend again.  You guessed it... Rake.

	rake db:seed

Go and refresh the page.  You should see a few more hackers to test with.

### Validation
This is starting to function pretty well for a few lines of code.  Next we are going to probably want to do some quick server side validation. Luckily for use Rails makes this dead simple.  We will want to validate on the model since that is the gatekeeper to the database.

To start open app/models/hacker.rb.  Update it with the following code.  Basically you are adding two "validates" lines to the model.

	attr_accessible :first_name, :email, :last_name
	validates :first_name, :last_name, :email, presence: true
	validates :email, uniqueness: true
	
The first will validate that all the fields are not empty.  It does a "presence" test.  That's pretty neat.  However, the second validation is where rails shines.  There is a "uniqueness" validation which will make sure that the email address submitted is unique.

### Style with Bootstrap

To start open your Gemfile and add the following lines

	gem "therubyracer"
	gem "twitter-bootstrap-rails"

Then from the command line run

	bundle install

This should install the new bootstrap gem.  You can think of gems as plugins for rails. Like a jQuery plugin.

Next, we need to install the bootstrap theme.

	rails g bootstrap:install
	rails g bootstrap:themed hackers -f

Finally, we can update our application layout to be a bit more exciting.  To do this we can replace the views/layouts/application.html.erb file with the following.

	<!DOCTYPE html>
	<html>
	<head>
	  <title>Hackathon</title>
	  <!--[if lt IE 9]>
	    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js" type="text/javascript"></script>
	  <![endif]-->
	  <%= stylesheet_link_tag    "application", :media => "all" %>
	  <%= javascript_include_tag "application" %>
	  <%= csrf_meta_tags %>
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
	  <div class="navbar navbar-fixed-top">
	    <div class="navbar-inner">
	      <div class="container">
	        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
	          <span class="icon-bar"></span>
	          <span class="icon-bar"></span>
	          <span class="icon-bar"></span>
	        </a>
	        <a class="brand" href="#">Hackathon</a>
	        <div class="nav-collapse">
	          <ul class="nav">
	            <li><%= link_to "Browse Hackers", hackers_path %></li>
	            <li><%= link_to "Menu Item 2" %></li>
	            <li><%= link_to "Menu Item 3" %></li>
	            <li><%= link_to "Menu Item 4" %></li>
	          </ul>
	        </div>
	      </div>
	    </div>
	  </div>

	  <div class="container">
	    <div class="row">
	      <div class="span9"><%= yield %></div>
	      <div class="span3">
	        <h2>About Us</h2>
	        We are learning how awesome and fun it can be to write an entire appliation riding on ruby on rails.  We learned how to configure and install rails, generate models, views and controllers, create and migrate a database, seed the database with test data, add validation, and style with a css framework bootstrap.
	      </div>
	    </div>
	  </div>
	</body>
	</html>

To see our updates shutdown the rails server using Ctrl-C. Then restart the server and reload your page.

	rails server

#Advanced Topics

# Testing and TDD

### Setup

#### Add testing Gems to your Gemfile
	group :test, :development do
	  gem 'rspec-rails' # testing framework
	  gem 'capybara' # user simulation
	  gem 'guard-rspec' # automatically run tests w/o the need for rake commands
	  gem 'growl' # growl notifications for test results
	end

#### Install the gems
	bundle install

#### Initialize Rspec
	rails generate rspec:install

#### Initialize Guard for Rspec
	guard init rspec

### Test our setup
	guard

### Create our first test
	rails generate integration_test users

At this point we should have some test that were run, assuming you left Guard running, but they should all fail. This is good.

### Update config/routes.rb
	resources :users

### Generate a controller/action and a model
	rails generate controller Users index
	rails generate model User first_name:string last_name:string email:string

### Create the database

	rake db:create

Let's not forget to migrate our Test database, since this is where the magic happens.

	rake db:test:prepare

This should be everything you need to get up and running with TDD in Rails. Now write some tests!

### Testing repos

+	[Base Application](https://bitbucket.org/rarneson/rails-hackathon-tdd-base) - Use this application if you want to start with a fresh application with all of the necessary gems already included.
+	[Full Application](https://bitbucket.org/rarneson/rails-hackathon-tdd) - Use this application if you want to see the final product with working tests for all CRUD methods.

### Testing Resources
+	[Net Tuts - The Intro to Rails Screencast I Wish I Had](http://net.tutsplus.com/tutorials/ruby/the-intro-to-rails-screencast-i-wish-i-had/)
+	[RailsCasts - How I Test](http://railscasts.com/episodes/275-how-i-test)
+	[How I learned to test my Rails applications](http://everydayrails.com/2012/03/12/testing-series-intro.html)
+	[Up and Running with Rails and TDD](http://ryanarneson.com/blog/2012/04/07/up-and-running-with-rails-and-tdd/)
+	[Capybara DSL](https://github.com/jnicklas/capybara)

## Rails Resources
+	[http://guides.rubyonrails.org/getting_started.html](http://guides.rubyonrails.org/getting_started.html)
+	[http://net.tutsplus.com/tutorials/ruby/the-best-way-to-learn-ruby-on-rails/](http://net.tutsplus.com/tutorials/ruby/the-best-way-to-learn-ruby-on-rails/)
+	[http://railscasts.com/](http://railscasts.com/)